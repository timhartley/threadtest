/**
 * Creates a number of threads, throttled to a specific count.
 * Provokes "thread count > MAX_THREADS, exiting !" error.
 * 
 * Create class file:
 * javac ThreadTest.java
 * 
 * Run:
 * java -cp ./ ThreadTest
 * 
 *
 */
public class ThreadTest {
	
	/** Total number of threads to create */
	private static final int THREAD_COUNT = 8192;
	
	/** Maximum permitted number of concurrently executing created threads. */
	private static final int MAX_CONCURRENT_THREADS = 4;
	
	/**
	 * Total computed sum which when output prevents the JIT compiler
	 * from aggressively optimising away the code.
	 */
	private static volatile double sum = 0.0d;

	/** */
	private static volatile int runningThreadCount = 0;
	
	private static void performComputation() {
		double d = 0.0d;
		for (int i = 0; i < 10000; i++) {
			d += Math.random();
		}
		sum += d;
		runningThreadCount--;
	}
	
	public static void main(String[] args) throws Exception {
		
		for (int i = 0; i < THREAD_COUNT; i++) {
			if (i % 100 == 0) {
				System.out.println("Created: " + i + " threads, " +
						runningThreadCount + " running.");
			}
			while (runningThreadCount >= MAX_CONCURRENT_THREADS) {
				Thread.sleep(10);
			}
			new Thread(new Runnable() {
				@Override
				public void run() {
					performComputation();
				}
			}).start();
			runningThreadCount++;
		}
		// Output sum to prevent possible dead code elimination
		System.out.println("SUM=" + sum);
	}
}
